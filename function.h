#ifndef FUNCTION_H
#define FUNCTION_H

// Расширенный способ проверка двух double чисел на равность по Epsilon. True - равны
bool approximatelyEqualAbsRel(double a, double b, double absEpsilon, double relEpsilon);

#endif // FUNCTION_H
