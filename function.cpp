#include <cmath>

bool approximatelyEqualAbsRel(double a, double b, double absEpsilon, double relEpsilon)
{
    // Проверяем числа на их близость - это нужно в случаях, когда сравнимые числа - нулевые или рядом с нулем.
    double diff = fabs(a - b);
    if (diff <= absEpsilon)
        return true;

    // В противном случае возвращаемся к алгоритму Кнута
    return diff <= ((fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * relEpsilon);
}
